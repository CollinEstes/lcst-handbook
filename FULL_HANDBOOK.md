# Welcome to the Team

This is the official handbook of the League City Barracudas swim team.  The intent of this handbook is to inform swimmers and  parents of general information about the team and the  season, as well as our relationship with Clear Creek Swim  League and our expectations in regard to safety, conduct  and participation. Familiarity with the information herein  will make the family’s participation more enjoyable.

This handbook does not include Clear Creek Swim  League rules and regulations, rules for intra-league  competition, technical rules for properly executing  strokes, cut-off times (Reserve and Champ), or league  records. This information is available in the current Clear Creek Swim League Handbook, which can be found on the CCSL website.  
  
A copy of this handbook, as well as other useful information, may be accessed on the team website www.lcbarracudas.org.
  
## Parent Meeting

An informative meeting will be held on Wednesday 5/10/2023 pm under the awning at Hometown Heroes Pool. We encourage all new parents/guardians as well as returning parents to attend.

# Overview

The League City Barracuda Swim Team is a non profit, tax-exempt organization, with a mission to field and support a competitive summer swim team. Our goal is to provide a quality program for the promotion, growth, training, enjoyment, good sportsmanship, and participation of children and youth in the sport of swimming.

The management of the team is vested in the Board of Directors and its officers. In August, the Board begins planning for the next swim season. Board members  meet regularly to represent the team to the Clear Creek Swim League, manage the budget, evaluate and hire coaches, arrange for the pool and related facilities, procure and inventory equipment, coordinate  registration, schedule practices, and manage ordinary business and administrative functions of the team.

Beginning in May, coaches will hold daily practice after school to improve stroke technique and build endurance needed for competition. These practice sessions are broken down into appropriate age groups. As soon as CCISD schools are dismissed for the summer, practice will switch to mornings. Meets will begin in June and be held every weekend through the end of June/beginning of July.  Post-season meets for those swimmers that meet or exceed cut-off time standards will be held following the regular season.

# 2023 Barracuda Committee Chairs  

There are still positions available. If you would like to volunteer for any openings listed below or know of another skill that would benefit the team, please contact any board member.  
  
| Position | Name |
| -------- | --------- |
| Covid Coordinator |
| Champ Series Liaison |
| Concessions |
| Fun Fridays |
| Handbook Design/Layout |
| Merchandise |
| Pictures |
| Scoring |
| Season Kick Off Party |
| Set-up/Tear Down | Cullen Oubre |
| Sponsors |
| Spirit Nights |
| Stroke Clinic |
| Swimsuits/T-shirts/Caps |
| Team Representative | Stefani Taylor |
| Volunteers |
| Officials | Dan Bunge |
| Social Media/Advertising |
| Lifeguards |
| Coaches |
| CCSL Representatvies | Stacia Lauderdale & Paige Tucker |
| Banquet | Robyn Ethridge |
| Audit |
| Clerk | Lisa Bunge |

## 2023 Schedule  
  
Keep up to date with the latest news and changes at www.LCBARRACUDAS.org  
  
### Important Dates  
  
| Event | Date | Location | Information |
| -------- | --------- | --------- | --------- |
| Tryouts | April 24 & April 25 | Hometown Heroes Pool (HHP) | link to tryout info |
| First day of Practice | May 1 | Hometown Heroes Pool (HHP) | link to practice info |
| Parent Meeting | May 10 | Under the Awning at HHP | We will have a parent meeting @ 5:15 & @ 6:15.  You only need to attend one of these times.  This will be the only parent meeting of the season, please make every effort to attend.|
| Team Suit Sales | May 10 | Under the Awning at HHP | Swim Shops of the Southwest will have the Barracuda Team suit available to purchase at a discounted rate.  Team suits are encouraged, but not required.|
| Team Pictures | TBD | HHP | TBD |
| Barracuda Banquet | TBD  | HHP | TBD |  

### Meet Schedule  

| Meet | Date | Team |
| -------- | --------- | --------- |
| TBD | June 3 | LCST vs TBD |
| TBD | June 10 | LCST vs TBD |
| TBD | June 17 | LCST vs TBD |
| TBD | June 24 | LCST vs TBD |
| TBD | July 1 | LCST vs TBD |
  
*Swimmer check in at 6:30 am – Meets begin at 8:00 am  
Check website for directions and parking information.  
  
### Last Chance Meet  

July 3rd location TBD
  
### Champ Series
  
| CCSL Champ Series | Date | Location |
| -------- | --------- | --------- |
| Reserve Meet | July 8 | Location TBA |
| Champ Meet | July 9 | Location TBA |

Dates & Times are subject to change. Watch for info via emails.  See Meet section for more information regarding meets.

### PRACTICE SCHEDULE

PRACTICES ARE IMPORTANT Swimmers will benefit greatly by regular attendance at practice sessions. At practices, they receive  instruction in the competitive strokes and are given an opportunity to build physical endurance. Swimmers also can keep up with the latest news as information is distributed at practices.
  
No Barracudas are allowed in the pool unless they are participating in practice. No unattended children are allowed at any practice or swim meet. THE CITY  REQUIRES THAT SWIMMERS REMAIN OUT OF HOMETOWN HEROES PARK BUILDING AT ALL TIMES.
All practices are held at Hometown Heroes Pool. Practice is from Monday through Friday. Practice times are listed below. **THERE IS NO PRACTICE ON MEMORIAL DAY.**  

### Afternoon Practices  

**MAY 1st through MAY 26th**  
High School Kids 4:00 – 5:00 pm  
8 & Under 5:00 – 6:00 pm  
9-14 yrs (Non-High School) 6:00 – 7:00 pm  
  
### Morning Practices  

**May 30th through June 30th**  
13 & up - 7:30 – 8:30 am  
9-12 8:30 – 9:30 am  
8 & unders 9:30 – 10:30 am
  
### Practices for Last Chance, Reserve & Champ Meet  

**July 3rd through July 7th**
13 & Up - 8:00 - 9:00 am
12 & Under - 9:00 - 10:00am
  
# 2023 League City Barracuda Board of Directors  

The names, email addresses and telephone numbers of the 2023 League City Swim Team Board of Directors are provided below. If you would like to volunteer in any way, please contact any board member below.  

| Position | Name | Email | Phone |
| -------- | --------- | --------- |--------- |
| President | Stefani Taylor | stefani@lcbarracudas.org | 713-397-5110 |
| Vice President | Stacey Armstrong | stacey@lcbarracudas.org| 832-528-2345 |
| Treasurer | Cherie Oubre | cherie@lcbarracudas.org | 409-718-5148 |
| Secretary | Robyn Ethridge | robyn@lcbarracudas.org | 281-381-8220 |
| Clerk of Course | Lisa Bunge | cudaclerk@gmail.com | 832-444-4732 |
| Officials | Dan Bunge | bunged_1@yahoo.com | 832-444-4732 |
| At Large | Amanda Cerelli | amanda@lcbarracudas.org | 619-316-7943 |
| At Large | Ashley Gremillion | ashley@lcbarracudas.org | 832-385-6963 |
| At Large | Charles Thompson | charles@lcbarracudas.org | 832-709-8835 |
| At Large | Cullen Oubre | cullen@lcbarracudas.org | 409-718-5144 |
| At Large | Lauren Estes | lauren@lcbarracudas.org | 409-392-6722 |

# Letter from the President

**Dear Barracudas,**

Welcome to the 2023 League City Barracudas Swim Season!  My name is Stefani Taylor and I am proud to be serving as your Barracudas President this year.  This is an extremely exciting time becoming the President as I have a long standing history with the Barracudas. I swam as a Barracuda from age 6 to 18, was a Volunteer Coach while in High School, a College Coach every summer I came home, then became the Assistant Coach for 2 years before becoming Head Coach until the summer of 2013 before I had my son, Klein. My parents were Board Members and both served as Presidents while my siblings and I swam, so this is fun coming full circle and being on the parent side instead of the swimmer/coach side.  My family sees the Barracudas as a way to have an extension to our family, give back to our community, and we look forward to it every year. 

Your fantastic 2023 Barracuda Board members have been meeting monthly since August to prepare for this swim season, and we are excited to see all of our “Barracuda Family” back at the pool this summer. We strive to make it the best experience for both parents and swimmers alike. If you are new to the team, welcome to the family!  The friendships you make while being a Barracuda will last a lifetime. If you have any questions at any time, please let me or any board member know.  During the season I am available by phone, text, or email. 

This year will host 2 to 3 swim meets (as soon as we know, I promise you will know 🙂). In order to make these meets function, we need you to complete your volunteer sign ups. Our volunteers are what make this team run! We can’t possibly do this without you! Go to our website, www.lcbarracudas.org and click the parent portal tab. You will see the volunteer opportunities for meets as well as our concession needs through this tab on the right side of the screen.

Please be sure to read this handbook thoroughly.  A few important reminders include:

- During the season we send out a weekly email called “Barracuda Bites” which contains important updates about what is happening with the team.  Make sure to check your email frequently.
- No parents or siblings will be permitted on the deck at any time during practices.  You are permitted under the awning or outside the fenced area (EXCEPT during 8 & under practice where someone MUST be within the gate at all times).  
- No child 8 and under can be dropped off.  Parents MUST park and walk swimmers in and remain on the premises for the duration of the 8U swimmers’ practice.  
- Entrance to the pool: All swimmers and parents MUST walk through the building to get to the pool.  
- Sidewalk chalk is NOT permitted per city guidelines.
- Please clean up after your family at practices and meets.  
- The baby pool is completely off limits during practices and meets. No Barracuda swimmers are allowed in the baby pool at any time.
- Our volunteers are what make this team run!  We can’t possibly do this without you!  Go to our website, www.lcbarracudas.org and click the parent portal tab.  You will see the volunteer opportunities for meets as well as our concession needs through this tab on the right side of the screen. If you do not have your volunteer requirements prior to the beginning of meet season, your swimmer will not be entered into the meets.
- Meet parking is in the big parking lot by the play set/soccer fields.  NO PARKING is permitted under the driveway at the front doors.  During meets only, pool access will be via the side gate closest to the play set. There is absolutely NO PARKING on the grass at any time, please adhere to this so we can keep the grass growing.

I am looking forward to a fantastic season!

**Stefani Taylor**  
**President, LCST Barracudas**  
**Cell: 713-397-5110**  
**Email: stefani@lcbarracudas.org**

# Barracuda Bites

The Barracuda Bites newsletter is your weekly connection to all that is going on with the Barracudas  during that week! Your family will receive an email each  week of the season with the Bites attached. Bites are also uploaded to the website under the Documents tab.  If you have questions on the Bites, please see a board member at the pool.

## CLEAR CREEK SWIM LEAGUE, inc

**CCSL**  
The Clear Creek Swim League, Inc. (CCSL) is a nonprofit organization formed in 1964 for the purpose of  sponsoring competitive and developmental swimming  events and activities which began in the Clear Creek  area and has now expanded throughout the Bay Area.  The Barracudas are members of the CCSL and are governed by its rules and regulations.
  
## WHAT DOES THE CCSL DO?
  
The CCSL board of directors performs the work of the league. They determine meet schedules, rules of competition, cut-off times and member team division  assignments. CCSL obtains liability insurance for  member teams, determines the league’s budget and  collects applicable fees from member teams. CCSL provides all ribbons, medals, clinics, etc. used by member teams. During the season, designated CCSL board members maintain each division’s official team rosters and notify other teams of changes. These rosters determine whether swimmers are eligible to participate in particular meets. Finally, CCSL keeps official score sheets for each meet and maintains league swim records.

> For more information visit the [CCSL’s website here](https://teampages.com/organizations/3982)  

## CCSL HANDBOOK

One of the most important CCSL actions is the publication of the CCSL Handbook. The handbook includes the rules that govern the conduct of meets, the order of events at the meets, the reserve and championship cut-off times, proper stroke execution rules, CCSL record-setting swims and much more. You can find a link to this handbook on their website.
  
## CCSL MEETS
  
Each competing CCSL team participates in five regular season meets. These meets generally match two teams in the same division. Following the regular season, a  Champs League Meet is held. Typically held the end of June or early July, this meet consists of three separate sessions: the Last Chance Meet, the Reserve Meet and the Championship Meet. Eligibility for the Reserve and Championship Meets are earned by achieving the cut-off  times published in the CCSL Handbook in one or more strokes.
**Entry fees will be charged for these meets per events that are swam.**  
  
## LAST CHANCE MEET
  
This meet is designed to allow swimmers who are close to achieving a cut-off time for the reserve or champ meet one “Last Chance” following the completion of the regular season to make the time before the Reserve and Championship Meet.  

# Need to Speak to a Coach?  

If you have any questions or concerns during practice or at a meet, we ask that you please direct them to a board member. The coaches are responsible for a large number of kids at practices and meets and will not be able to provide the assistance you need. If it is a coaching issue, the board will go to the appropriate coach for your answers.
  
Coach Berg: coachberg@lcbarracudas.org  
Coach Hannah: coachhannah@lcbarracudas.org

# Discipline Problems  

Swimmers must maintain good self-discipline. During practices or meets, if a swimmer misbehaves excessively,  a coach and/or board member will contact the swimmer’s parents. If the swimmer’s behavior does not improve,  he/she will be dropped from the team. Under this  circumstance, registration fees will not be refunded. This is entirely at the discretion of the board and/or coaches.

# Equipment

**TEAM T-SHIRTS**
All Barracuda swimmers will receive a team T-shirt with  their registration. Team T-shirts will be available for  purchase after the first week of practice to ensure all of  our swimmers have received one with their registration. Be aware of sizes when registering you swimmer.  Youth and adult sizes are available.

**SWIM CAPS**  
Only LCST swim caps may be worn during swim meets.  Each swimmer will be given one before the first meet of the season. Additional caps are available for purchase under the Barracuda tent throughout the season.  

**SWIMSUITS**  
The team has chosen competition swimsuits, which  may be worn to practices and meets. These particular  suits are NOT required; but they do conform to swimming rules, look great and provide less drag than commercial suits. **While the team suit is optional, the team requires girls to wear one-piece swimsuits suitable for competition to all practices and meets.**
Swimsuit sales will be scheduled, and an email will be sent with information about when the vendor will be at the pool during practices.

**ACCESSORIES**
At all swim meets, caps & goggles will be available for sale. Only a Barracuda swim cap may be worn at swim meets. Barracuda merchandise will be for sale under the Barracuda tent.

# Inclement Weather
  
In the event of stormy weather, remember 30-30. We evacuate the pool when lightning to thunder is within 30 seconds and we stay out of the pool until 30 minutes after the last rumble of thunder.

If a practice is cancelled or postponed due to inclement weather you will be notified via text message.  To receive text messages regarding practices, etc, you must have a cell phone listed on your account. Be sure all cell phone numbers are listed to ensure you are all receiving the texts.

## The Legend of the Cabbage  
  
Once Upon a time, in the far away land of League City, there swam some very excited and energetic Barracudas. They would practice every day, trying to get reserve and champ times and win the most points they could for their team.  Across from their pool, there was an old farmer, who planted all sorts of lettuces, but most of all cabbage. He planted so much cabbage that he could not pick them all and the cabbage began to rot and smell. As the little Barracudas were practicing day in and day out, they began to smell the strong odor coming from the farmer’s field, and they began to swim faster and take fewer breaths, desperately trying not to smell the cabbage.  

Soon, the Barracudas realized that the cabbage smell had helped them become very speedy swimmers and win many championships. To this day, all Barracudas remember how “cabbage power” helped them to become the fastest swimmers in the land.
  
**The Barracudas are bound with Cabbage Power and  we give our swimmers cabbage before every meet.**
  
## Barracuda Team Cheers  
  
**DUNK-A-HEAD**  
Dunk-a-head, dunk-a-head
Black and red  
Dunk-a-head, dunk-a-head
Cabbage fed.  

**DAFFY DUCK**
My name is Daffy Duck
I live on a merry go round
I play all day, in the merriest way
And this is how it sounds
Toot, toot, toot, toot!
  
**DYNAMITE!**  
Coaches> Barracudas are what?  
Kids>Dynamite  
Coaches> Barracudas are what?
Kids>Dynamite
All> Barracudas are...
Tick tick tick tick, **BOOM Dynamite!**

**UH UNGOWA**
Uh, ungowa, we got that cabbage power
*repeated many times with a dance*

# Family Meet Packing List

Swim meets typically last between **5 and 6 hours** in some very hot Houston conditions.  It is highly advised that families plan accordingly and pack items to stay cool, hydrated and safe from the sun.  Below are some suggested items for your swim meet "camp".

## Shade / Seating

Probably the most important is having proper shade for you and your family between races.  It is highly **recomended to have a canopy tent large enough for your whole family** and enough chairs for everyone to have a nice shady seat.

## Sun protection

With swim meets lasting until mid day, it is highly recommended that you have sun screen or sun protective clothing for yourself and your family.  While the Barracudas try hard to keep the swimmers in shaded areas as much as possible, all swimmers will still have fairly long periods of time with direct summer sunlight exposure.  Plan accordingly.

## Water / Snacks

All home swim meets and most all away swim meets feature concession stands with drinks, snacks and food choices.  In addition however familes are encoraged to bring extra water to make sure everyone stays safe and hydrated.  Our swim meets can get extermely hot and staying hydrated is critical to keeping you and your family safe. In addition our meets will run through a normal lunch time so you may also want to pack healthy snacks like fruits or nuts for your swimmers.

## Swim Gear

In addition to the items to keep your family safe and comfortable between events, it is also good to make sure you have the following items for each of your swimmers:

- Googles (including a backup pair)
- Towel
- Cap
- Team shirt

## Example Meet Packing List
  
- 10 x 10 canopy tent is highly recommended!
- Towels
- Goggles (Bring extra pairs)
- Spray bottle w/ water (It gets HOT!!)
- Swim Cap
- Barracuda Shirt
- Sunscreen
- Bug Spray
- Sharpie Marker
- Water
- Drinks
- Flip Flops
- Hairbrush
- Spray Conditioner
- Koozie
- Ponytail holders  
- Sunglasses
- Visors
- Towel Clips
- Blanket  
- Small pillow
- Chairs
- Games
- Books  
  
# Meets  
  
## HOW TO REGISTER FOR A MEET  
  
To register for a meet, go to the team website and click  on the **Parent Portal** tab. Here you will be directed to swim manager. It may take a minute for the page to load, and you may be required to sign in with your username  and password. Once in this system, you will see Meets across the top. Click that tab and work your way through the registration process for each meet. Our coaches want you to accept or decline attending each meet. This enables them to create meet entries knowing that your swimmer(s) will or won't be able to attend.

<!-- ![Screen shot of parental portal](/images/viewmeets.jpg)
  
![Screen shot of selection and save process for meets](/images/attendingmeet.jpg) -->
  
**For your swimmer to participate in a meet, you  must register your swimmer online by 9:00 PM  the SUNDAY before the Saturday swim meet.** If you miss the 9:00 PM sign up deadline, you MUST e-mail Coach Berg (aquaticscoach8283@gmail.com) and our clerk of course, Lisa Bunge (cudaclerk@gmail.com) to see about getting entered into the meet. You cannot  register online at that point.
  
On the registration page there is a space for notes. This is to be used ONLY if you are arriving late or have to leave the meet early. The notes space is not to be used for requesting strokes or events for your swimmer. If you would like to request something for your child to swim, please email Coach Berg.  
  
Please know that the decision of what a swimmer swims in the meet is ultimately up to the coaches. We have complete confidence in our coaches, and know that they will put each child in the events for which they are best suited or in events that will be most beneficial for the team.
  
## GOING TO MISS A MEET?

Many hours go into scheduling our swimmers for  meets. It is imperative that you DECLINE attendance at a meet you know you will not be able to attend.  Coaches submit all entries to the opposing team by the Tuesday preceding the meet. Your ACCEPT/DECLINE for each swimmer for each meet will help ease the meet entry process for our coaches. Entries are generally posted at the pool by Thursday. Failure to notify the coach may result in consequences set forth under the Entries Section.  

In the event of an emergency or sickness the day of the meet, please contact the Clerk of Course by phone or email to notify us of your absence so that your entry can be pulled and not count against our meet.
  
## MEET ARRIVAL TIME
  
All swimmers should arrive at the meet pool by the designated time for check-in and warm-ups. Late arrivals can lose their place in an event. You must have your swimmer check-in with a coach and a parent needs to check-in at the Barracuda tent.

## LEAVING A MEET  
  
It is imperative that a coach and the clerk of course be notified of any early departure. Please remember that if your child swims on a relay, his/her absence will affect 3 other swimmers. Please don’t leave 3 other teammates, who may have waited hours to swim, hanging.

Also remember, sometimes your child may be placed in an individual or relay event as a replacement swimmer during the meet or may have forgotten that they are swimming in the last relay. Please check with the Clerk of Course before actually leaving.
  
## STAY WITH THE TEAM AT THE POOL
  
During meets, swimmers should stay near the team in the designated Barracuda area if one is provided.  They will be notified when it is time for them to report to the ready area for an event. When swimmers arrive at the ready area, they must remain in the Ready Area. Swimmers will then be assisted in getting in their appropriate heats. Ultimately, it is the swimmer’s responsibility to be in the ready area at the appropriate time or they could be scratched from the event and possibly the meet. If we all stay together it is much easier to get our team where we need to be!  
  
## QUESTIONS OR COMPLAINTS

During swim meets, any questions or complaints about actions of the officials or the opposing team must be directed to the Barracuda Team Representative, who  will relay the question or complaint to the Referee. If you have a question or complaint during a meet, go to the Barracuda Tent and ask to speak to the Team Representative.  They will be happy to advocate for you your swimmer.  
  
CCSL rules prohibit parents or swimmers from approaching Judges and/or the Referee directly; only an official league representative may do so. Disregarding this rule could cause the team to incur sanctions or prompt your removal from the competition. Refer to the  disqualification section for further explanation. Please represent yourself, your swimmer and the team well by being courteous and respectful of officials as well as all other meet volunteers.
  
## EVENTS
  
At meets, swimmers compete against other swimmers of the same sex, age group, and general ability. There are six age groups: 6 and Under, 8 and Under, 9 & 10, 11 & 12, 13 & 14, and 15 - 18. A meet includes competition in seven event types. There are five individual events:  Freestyle, Backstroke, Breaststroke, Butterfly, and Individual Medley. There are two relays: Freestyle Relay and Medley Relay.

Each age group participates in each event type with exception of the following:  

- ages 6 and under do not swim Butterfly, Breaststroke, Individual Medley, or Individual  Medley relay
- ages 9 and under do not swim the Individual Medley  

All Barracuda swimmers are eligible to swim five events per meet: three individual events and two relays.
  
## ENTRIES

While a swimmer may be entered in a maximum of five events per regular season meet, the Coach may not be able to enter all swimmers in five events. An entry list, which identifies the events each swimmer is entered in, will be posted by Thursday. Any swimmer not swimming in a meet must notify the coach in writing by Monday preceding the meet. Due to team size and time constraints at other pools, we may have to limit the number of swimmers that we bring. We will do everything possible to ensure that every child will have a chance to swim in at least two events at home meets.
  
## RIBBONS

All swimmers are awarded ribbons in all heats in regular season meets except for disqualifications which  will depend on the home team that awards the ribbons.  The event and time will be recorded on the back of the ribbon. The ribbons will be distributed on the Tuesday after the meet during practices. Have a special place to show off your ribbons!!
  
## DISQUALIFICATIONS

Swimmers are judged according to the USA Swimming  Rules and Regulations for competition with the  following exception: one false start is allowed before  disqualification. Unfortunately, swimmers are sometimes disqualified during a race, usually for executing a  start, stroke, turn or finish illegally. They may also be disqualified for early takeoff in relays. The starter, stroke and turn judges, and the referee may disqualify  swimmers. It is important to remember that the meet referee can disqualify individual competitors for poor sportsmanship and conduct as well.
  
These officials are parent volunteers who have received specific CCSL training. Their purpose is to assure that no swimmer gains an unfair advantage over another swimmer.  When time permits, an official will seek out the swimmer or their coach to explain the reason for disqualification. It is a CCSL rule that only an official league representative approach an official regarding a disqualification. Do not, under any circumstance, do this yourself. Disregarding  this rule could cause the team to incur sanctions or prompt your removal from the competition.  
As with any concern, please approach a board member for assistance. Please represent yourself, your swimmer and the team well by being courteous and respectful of officials as well as all other meet volunteers.
  
## TIMES AND RECORDS
  
Swimmers should realize that they are competing against the clock as well as other swimmers in the water. Their goal should be to better their times at each meet. CCSL publishes a series of time standards for boys and girls by age groups that are used as a scale to gauge a swimmer’s level of achievement in each event. They are, from slowest to fastest, reserve and champ times.  These cut-off times are listed on the CCSL website  
  
Trying to reach the next level in a particular event can be very motivational to swimmers. Learn to read the cut-off time charts with your swimmers.  
During the week following a meet we award a Swimmer of the Meet. These swimmers are awarded a Swimmer of the Meet t-shirt which is based on the swimmer in an age division that drops the most time in a race... so swim hard and fast.

## Team Membership Fees

- **Swimmer**: $175.00 each
- **Active High School Swimmers**: $110.00/each
- **Active USA Swimmers**:  $110.00/each
(plus $5 Hytek Administration Fee/Athlete)

The maximum fee is $500.00 per family. (more than 3 swimmers)

A late registration fee of $25.00 is charged for each swimmer whose fees are not paid by the first day of practice.
No swimmer with unpaid registration fees may practice or swim in a meet.

Only swimmers having paid their fee will be added to the official team roster. This roster is an official league document. League rules prohibit entrants not on a team roster.  
  
## Tryouts
  
All new swimmers **must** try out for the team. In order to join, a swimmer must be able  to safely swim 25 yards unassisted. The swimmer’s ability will be judged solely by the coaching staff and/or team board. Try-outs are not needed for any competitive USA registered swimmer or a swimmer that has been on another CCSL team.

Tryouts for the 2023 season will take place on Monday and Tuesday, April 24th  & 25th at Hometown Heroes Pool from 5-6:30 pm. (only need to attend one)  

- Entry to pool will be via the front of the building.  
- Proof of age required – photo of birth certificate on phone is acceptable.
- Swimmer will need to come wearing suit and ready to get into the pool.
- Swimmer should bring towel and goggles.
- Swimmer will swim per coaches' instructions.
- Swimmer will be free to leave once the tryout is completed.

## Swimmer's Age  
  
A swimmer’s age for purposes of competition is his age on May 1st. A swimmer does not change age groups during the season. All new swimmers must be at least  six (6) years of age, as of May 1st, and be able to swim the required 25 yards. We will be accepting five (5) year old siblings of any returning Barracuda families. Proof of age will be required for new swimmers.
  
## Refunds

Refunds will be made through the first week of practice with a $30 administrative fee per swimmer deducted.  After that, no refunds will be paid.

# Safety Guidelines

The safety and well being of your child is of the utmost concern to us. The following guidelines have been set to help ensure their safety. Please discuss these rules  with your children so that everyone understands what is expected.

## Swimmers 8 & Under

1. Children may not be dropped off and left alone at  the pool.
1. If you cannot be with your children, please make  arrangements with another parent or guardian  (at least 15 years old) to look after them during a  practice or meet. Swimming is different from most  team sports in that there are many more children  than coaches. Please know where your child is at  all times, including when in the water.

## All Swimmers

1. If dropped off, go immediately inside the fence to the  pool area. All children must stay in the fenced pool  area during practice.
1. Do not climb on the lifeguard stands. Hometown  Heroes Pool rules must be observed at all times.  
1. All trash must be picked up after practices and home  
meets. The City will be charging the team for clean  up if this is not done.
1. A swimmer should inform one of the coaches if he/ she leaves the pool to go to the restroom or must  leave practice early. Only restrooms at the pool are  to be used.
1. Swimmers must not enter the pool before their  scheduled practice time and should get out as  soon as their age group is released unless given  prior permission by the head coach to start early or  remain late.
1. Swimmers are not allowed in the Hometown Heroes  Park building when wet. This is a city rule!
1. Please be prompt in picking up your swimmer. For  their safety, you must come inside the fenced  pool area to get them. This brief visit also helps  parents keep up with team information.
1. During a meet, no one is allowed in any part of the competition pool unless they are participating  in an event or using a warm up lane, if provided  and supervised. This includes dangling feet in  areas not used for competition. Failure to follow  this rule could result in your swimmer being  removed from the meet.
1. No alcoholic beverages or tobacco products are  allowed at practices or meets.

## Baby Pool

With City enforcement that both pools require lifeguards, the Baby Pool will **NOT** be open during practices or meets. No parents, swimmers, or siblings are allowed in the Baby Pool/Splash Pad at any time.

# Spirit Nights

Let’s gather together and show our Barracuda pride while supporting local businesses. A percentage of your purchase will go back to the Barracuda’s. Make sure you tell everyone you know to support the Barracuda’s!

A complete list of Spirit Nights will be emailed and put in the Barracuda Bites Newsletter

# Stroke Clinic  

Stroke Clinic is a great way to get a more personal training to develop, strengthen or correct your swimmer’s swim strokes. The coach to swimmer ratio is 1:5. Payment is due prior to the start of the clinic. Cost is $45 for the 4 day clinic. Space is limited to 25 swimmers. Clinics are Mondays - Thursdays at the Hometown Heroes pool.  Registration will be made available online.
  
Stroke Clinic I June 5-8 10:30 – 11:30 am
Stroke Clinic II June 12-15 10:30 – 11:30 am
Stroke Clinic III June 19-22 10:30 – 11:30 am  

*Friday of each week will be used as a make-up for any day cancelled due to inclement weather.

# Team Pictures

We are currently working with our photographer on dates for individual and team photos this year.  You will be notified by email with all updates.

# Volunteers

Because there are so many jobs to be done, parent  participation is **mandatory** at a swim meet. The  Barracudas must furnish more than 75 workers for each  away meet and at least 125 workers for each home  meet. Every family is asked to volunteer at least 3 times during the swim season. Families with more than one  swimmer will be responsible for volunteering 5x during the season. An email will be sent out inviting your family to register for various volunteer shifts. Once you have selected your 3 choices, your registration status will be approved. The week prior to your shift, you will receive an email reminder. You must check in with the volunteer coordinator prior to the start of each swim meet. If you fail to show up for your designated shift, your child may not be able to swim in the next swim meet.  

<!-- ![Screenshot of Volunteer Selections](/images/volunteershifts.jpg) -->

## Family Requirements

Families are asked to volunteer during the swim season as follows:

- **Single Swimmer Families**: at least 3 times
- **Families**: at least 5 times

NO CHILD WILL BE CONSIDERED REGISTERED AND ABLE TO START PRACTICE UNTIL YOUR REGISTRATION, VOLUNTEER FORM AND PAYMENT ARE RECEIVED!

## Volunteering Process

### Selection and Registration

An email will be sent out inviting your family to register for various volunteer shifts. Once you have selected your 3 choices, your registration status will be  approved.

### Week of the Meet

The week prior to your shift, you will receive  an email reminder. You must check in with the volunteer coordinator prior to the start of each swim meet.

If you fail to show up for your designated shift, your child may not be able to swim in the next swim meet.  

## Positions

Positions, which must be filled at a swim meet, are briefly described below. These positions are divided into  three groups, depending on the amount of training or experience that is needed.

Swimming is a very volunteer-oriented sport. Our children  would not get to enjoy this wonderful sport without the  efforts of their parents. One of the crucial roles parents play is officiating. The reason I originally started officiating is that I wanted the best spot on the deck to watch my kid swim. You can't get any closer! The other benefit that the Barracudas give is the relief from any other volunteer requirements. That's right, if someone in your family  officiates, you don't have to volunteer for anything else!  

**The training clinics are FREE!**

## Trained Positions

For the following jobs, specialize training is required.  The CCSL provides clinics in the spring to train parents  to fill these positions. They require a rather complete  understanding of competitive swimming.

Not up for training? Check out the [No Experience Required section below](#no-experience-required-positions)

### Team Clerk of Course

Handles changes to meet entry cards, entry lists,  and heat assignments resulting from scratches and  substitutions. The Clerk of Course also determines  the legality of swimmer entries for both teams, and  helps distribute entry cards to swimmers.

### Stroke and Turn Judge

Judge the technical form of strokes and turns  according to League rules and disqualify swimmers  for violations. Must wear solid white shirts or CCSL  official shirt and navy blue shorts/skorts.

### Starter

Calls swimmers to the starting block, starts each  heat, and handles disqualifications on starts. The  Starter can also work as a Stroke and Turn Judge at  the direction of the Referee. Starter positions require  prior certification and experience as a Stroke and  Turn Judge.

### Referee

Is the senior official at a meet and has full and final  authority in all decisions and rule interpretations.  Referee positions require prior certification and  experience as a Stroke and Turn Judge and Starter.

#### League Training Clincs

Our primary job is to make sure the competition is fair  for all of the swimmers. We do that by making sure the  swimmers are doing the strokes as the rules state and  that no one gets an advantage. There is a training session  and test involved.

## No Experience Required Positions

The following positions require **no previous experience or training**:

### Meet Volunteer Coordination Team

Coordinates the staffing and schedule of all volunteers necessary for swim meets.

### Set up and Tear Down

Help prepare the pool area for the swim meets and pack away after meets (home meets only). Coordinate clean up of the pool area after home meets. (All Team members and parents are asked to help clean up after a home meet.)

### Concession Stand

Help prepare orders and/or collect money. Also may be asked to deliver cold drinks to officials and workers during the meet. The last shift of the day helps clean the refreshment area (home meets only).

### Runners

Responsible for collecting lane sheets and DQ slips from the timers and deliver them to the scorers.  Also be responsible to help moving the 8 and under swimmers to the other side of the pool for some heats.

### Ready Area

Help arrange, organize and prepare children for races.

### Head Timer

Coordinates the timers from both teams. Insures that  all lanes are filled with the proper number of timers.  At home meets will conduct a meeting in the morning  with both teams. The head timer also starts back-up stopwatches.

### Timers

Time swimmers using digital stop watches and record the time on the swimmer’s entry card. There are three timers per lane.

### Head Scorer

Coordinates data entry, validates records with referee,  resolves questions regarding DQs, validates scoring  
and certifies official meet results.

### Scorers

Enter swimmers’ official times, disqualifications,  and scratches in the meet management computer. Validate record performances, calculate team scores and post official heat results. You should be comfortable working with numbers and computers.

### Head Ribbon Person

Coordinates ribbon writing for all meets.  

### Ribbon Labeling Team

Receive labels from the scorers and place on the back of the ribbons for all swimmers to be filed and distributed at a later date.

### Stroke Clinic Coordinator

Promotes, schedules and coordinates participation in special small group clinics to improve stroke technique.

### Ribbon Distribution Team

Distribute ribbons during practices one day week following the meet.

### Champ Series Signup Team  

Assist swimmers in filling out entry forms and  making payments to enter the champ series meets.

### Banquet Team

Organize and arrange the awards banquet.

### Merchandise Team

Responsible for selling Barracuda items one  day a week at practice and at swim meets.
