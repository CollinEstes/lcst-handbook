---
weight: 9
bookFlatSection: true
title: "CCSL"
---
  
## CLEAR CREEK SWIM LEAGUE, inc

**CCSL**  
The Clear Creek Swim League, Inc. (CCSL) is a nonprofit organization formed in 1964 for the purpose of  sponsoring competitive and developmental swimming  events and activities which began in the Clear Creek  area and has now expanded throughout the Bay Area.  The Barracudas are members of the CCSL and are governed by its rules and regulations.
  
## WHAT DOES THE CCSL DO?
  
The CCSL board of directors performs the work of the league. They determine meet schedules, rules of competition, cut-off times and member team division  assignments. CCSL obtains liability insurance for  member teams, determines the league’s budget and  collects applicable fees from member teams. CCSL provides all ribbons, medals, clinics, etc. used by member teams. During the season, designated CCSL board members maintain each division’s official team rosters and notify other teams of changes. These rosters determine whether swimmers are eligible to participate in particular meets. Finally, CCSL keeps official score sheets for each meet and maintains league swim records.

> For more information visit the [CCSL’s website here](https://teampages.com/organizations/3982)  

## CCSL HANDBOOK

One of the most important CCSL actions is the publication of the CCSL Handbook. The handbook includes the rules that govern the conduct of meets, the order of events at the meets, the reserve and championship cut-off times, proper stroke execution rules, CCSL record-setting swims and much more. You can find a link to this handbook on their website.
  
## CCSL MEETS
  
Each competing CCSL team participates in five regular season meets. These meets generally match two teams in the same division. Following the regular season, a  Champs League Meet is held. Typically held the end of June or early July, this meet consists of three separate sessions: the Last Chance Meet, the Reserve Meet and the Championship Meet. Eligibility for the Reserve and Championship Meets are earned by achieving the cut-off  times published in the CCSL Handbook in one or more strokes.
**Entry fees will be charged for these meets per events that are swam.**  
  
## LAST CHANCE MEET
  
This meet is designed to allow swimmers who are close to achieving a cut-off time for the reserve or champ meet one “Last Chance” following the completion of the regular season to make the time before the Reserve and Championship Meet.  
