---
weight: 11
bookFlatSection: true
title: "Meets"
---

# Meets  

![SWIM MEETS](/images/meets.jpg)
  
## HOW TO REGISTER FOR A MEET  
  
To register for a meet, go to the team website and click  on the **Parent Portal** tab. Here you will be directed to swim manager. It may take a minute for the page to load, and you may be required to sign in with your username  and password. Once in this system, you will see Meets across the top. Click that tab and work your way through the registration process for each meet. Our coaches want you to accept or decline attending each meet. This enables them to create meet entries knowing that your swimmer(s) will or won't be able to attend.

![Screen shot of parental portal](/images/viewmeets.jpg)
  
![Screen shot of selection and save process for meets](/images/attendingmeet.jpg)
  
**For your swimmer to participate in a meet, you  must register your swimmer online by 9:00 PM  the SUNDAY before the Saturday swim meet.** If you miss the 9:00 PM sign up deadline, you MUST e-mail Coach Berg (aquaticscoach8283@gmail.com) and our clerk of course, Lisa Bunge (cudaclerk@gmail.com) to see about getting entered into the meet. You cannot  register online at that point.
  
On the registration page there is a space for notes. This is to be used ONLY if you are arriving late or have to leave the meet early. The notes space is not to be used for requesting strokes or events for your swimmer. If you would like to request something for your child to swim, please email Coach Berg.  
  
Please know that the decision of what a swimmer swims in the meet is ultimately up to the coaches. We have complete confidence in our coaches, and know that they will put each child in the events for which they are best suited or in events that will be most beneficial for the team.
  
## GOING TO MISS A MEET?

Many hours go into scheduling our swimmers for  meets. It is imperative that you DECLINE attendance at a meet you know you will not be able to attend.  Coaches submit all entries to the opposing team by the Tuesday preceding the meet. Your ACCEPT/DECLINE for each swimmer for each meet will help ease the meet entry process for our coaches. Entries are generally posted at the pool by Thursday. Failure to notify the coach may result in consequences set forth under the Entries Section.  

In the event of an emergency or sickness the day of the meet, please contact the Clerk of Course by phone or email to notify us of your absence so that your entry can be pulled and not count against our meet.
  
## MEET ARRIVAL TIME
  
All swimmers should arrive at the meet pool by the designated time for check-in and warm-ups. Late arrivals can lose their place in an event. You must have your swimmer check-in with a coach and a parent needs to check-in at the Barracuda tent.

## LEAVING A MEET  
  
It is imperative that a coach and the clerk of course be notified of any early departure. Please remember that if your child swims on a relay, his/her absence will affect 3 other swimmers. Please don’t leave 3 other teammates, who may have waited hours to swim, hanging.

Also remember, sometimes your child may be placed in an individual or relay event as a replacement swimmer during the meet or may have forgotten that they are swimming in the last relay. Please check with the Clerk of Course before actually leaving.
  
## STAY WITH THE TEAM AT THE POOL
  
During meets, swimmers should stay near the team in the designated Barracuda area if one is provided.  They will be notified when it is time for them to report to the ready area for an event. When swimmers arrive at the ready area, they must remain in the Ready Area. Swimmers will then be assisted in getting in their appropriate heats. Ultimately, it is the swimmer’s responsibility to be in the ready area at the appropriate time or they could be scratched from the event and possibly the meet. If we all stay together it is much easier to get our team where we need to be!  
  
## QUESTIONS OR COMPLAINTS

During swim meets, any questions or complaints about actions of the officials or the opposing team must be directed to the Barracuda Team Representative, who  will relay the question or complaint to the Referee. If you have a question or complaint during a meet, go to the Barracuda Tent and ask to speak to the Team Representative.  They will be happy to advocate for you your swimmer.  
  
CCSL rules prohibit parents or swimmers from approaching Judges and/or the Referee directly; only an official league representative may do so. Disregarding this rule could cause the team to incur sanctions or prompt your removal from the competition. Refer to the  disqualification section for further explanation. Please represent yourself, your swimmer and the team well by being courteous and respectful of officials as well as all other meet volunteers.
  
## EVENTS
  
At meets, swimmers compete against other swimmers of the same sex, age group, and general ability. There are six age groups: 6 and Under, 8 and Under, 9 & 10, 11 & 12, 13 & 14, and 15 - 18. A meet includes competition in seven event types. There are five individual events:  Freestyle, Backstroke, Breaststroke, Butterfly, and Individual Medley. There are two relays: Freestyle Relay and Medley Relay.

Each age group participates in each event type with exception of the following:  

- ages 6 and under do not swim Butterfly, Breaststroke, Individual Medley, or Individual  Medley relay
- ages 9 and under do not swim the Individual Medley  

All Barracuda swimmers are eligible to swim five events per meet: three individual events and two relays.
  
## ENTRIES

While a swimmer may be entered in a maximum of five events per regular season meet, the Coach may not be able to enter all swimmers in five events. An entry list, which identifies the events each swimmer is entered in, will be posted by Thursday. Any swimmer not swimming in a meet must notify the coach in writing by Monday preceding the meet. Due to team size and time constraints at other pools, we may have to limit the number of swimmers that we bring. We will do everything possible to ensure that every child will have a chance to swim in at least two events at home meets.
  
## RIBBONS

All swimmers are awarded ribbons in all heats in regular season meets except for disqualifications which  will depend on the home team that awards the ribbons.  The event and time will be recorded on the back of the ribbon. The ribbons will be distributed on the Tuesday after the meet during practices. Have a special place to show off your ribbons!!
  
## DISQUALIFICATIONS

Swimmers are judged according to the USA Swimming  Rules and Regulations for competition with the  following exception: one false start is allowed before  disqualification. Unfortunately, swimmers are sometimes disqualified during a race, usually for executing a  start, stroke, turn or finish illegally. They may also be disqualified for early takeoff in relays. The starter, stroke and turn judges, and the referee may disqualify  swimmers. It is important to remember that the meet referee can disqualify individual competitors for poor sportsmanship and conduct as well.
  
These officials are parent volunteers who have received specific CCSL training. Their purpose is to assure that no swimmer gains an unfair advantage over another swimmer.  When time permits, an official will seek out the swimmer or their coach to explain the reason for disqualification. It is a CCSL rule that only an official league representative approach an official regarding a disqualification. Do not, under any circumstance, do this yourself. Disregarding  this rule could cause the team to incur sanctions or prompt your removal from the competition.  
As with any concern, please approach a board member for assistance. Please represent yourself, your swimmer and the team well by being courteous and respectful of officials as well as all other meet volunteers.
  
## TIMES AND RECORDS
  
Swimmers should realize that they are competing against the clock as well as other swimmers in the water. Their goal should be to better their times at each meet. CCSL publishes a series of time standards for boys and girls by age groups that are used as a scale to gauge a swimmer’s level of achievement in each event. They are, from slowest to fastest, reserve and champ times.  These cut-off times are listed on the CCSL website  
  
Trying to reach the next level in a particular event can be very motivational to swimmers. Learn to read the cut-off time charts with your swimmers.  
During the week following a meet we award a Swimmer of the Meet. These swimmers are awarded a Swimmer of the Meet t-shirt which is based on the swimmer in an age division that drops the most time in a race... so swim hard and fast.
