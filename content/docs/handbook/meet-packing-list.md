---
weight: 5
bookFlatSection: true
title: "Meet Packing List"
---

# Family Meet Packing List

Swim meets typically last between **5 and 6 hours** in some very hot Houston conditions.  It is highly advised that families plan accordingly and pack items to stay cool, hydrated and safe from the sun.  Below are some suggested items for your swim meet "camp".

## Shade / Seating

Probably the most important is having proper shade for you and your family between races.  It is highly **recomended to have a canopy tent large enough for your whole family** and enough chairs for everyone to have a nice shady seat.

## Sun protection

With swim meets lasting until mid day, it is highly recommended that you have sun screen or sun protective clothing for yourself and your family.  While the Barracudas try hard to keep the swimmers in shaded areas as much as possible, all swimmers will still have fairly long periods of time with direct summer sunlight exposure.  Plan accordingly.

## Water / Snacks

All home swim meets and most all away swim meets feature concession stands with drinks, snacks and food choices.  In addition however familes are encoraged to bring extra water to make sure everyone stays safe and hydrated.  Our swim meets can get extermely hot and staying hydrated is critical to keeping you and your family safe. In addition our meets will run through a normal lunch time so you may also want to pack healthy snacks like fruits or nuts for your swimmers.

## Swim Gear

In addition to the items to keep your family safe and comfortable between events, it is also good to make sure you have the following items for each of your swimmers:

- Googles (including a backup pair)
- Towel
- Cap
- Team shirt

## Example Meet Packing List
  
- 10 x 10 canopy tent is highly recommended!
- Towels
- Goggles (Bring extra pairs)
- Spray bottle w/ water (It gets HOT!!)
- Swim Cap
- Barracuda Shirt
- Sunscreen
- Bug Spray
- Sharpie Marker
- Water
- Drinks
- Flip Flops
- Hairbrush
- Spray Conditioner
- Koozie
- Ponytail holders  
- Sunglasses
- Visors
- Towel Clips
- Blanket  
- Small pillow
- Chairs
- Games
- Books  
  