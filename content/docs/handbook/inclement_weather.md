---
weight: 29
bookFlatSection: true
title: "Inclement Weather"
---

# Inclement Weather
  
In the event of stormy weather, remember 30-30. We evacuate the pool when lightning to thunder is within 30 seconds and we stay out of the pool until 30 minutes after the last rumble of thunder.

If a practice is cancelled or postponed due to inclement weather you will be notified via text message.  To receive text messages regarding practices, etc, you must have a cell phone listed on your account. Be sure all cell phone numbers are listed to ensure you are all receiving the texts.
