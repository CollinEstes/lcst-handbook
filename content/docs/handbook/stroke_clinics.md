---
weight: 10
bookFlatSection: true
title: "Stroke Clinic"
---

# Stroke Clinic  

Stroke Clinic is a great way to get a more personal training to develop, strengthen or correct your swimmer’s swim strokes. The coach to swimmer ratio is 1:5. Payment is due prior to the start of the clinic. Cost is $45 for the 4 day clinic. Space is limited to 25 swimmers. Clinics are Mondays - Thursdays at the Hometown Heroes pool.  Registration will be made available online.
  
Stroke Clinic I June 7-10 10:30 – 11:30 am
Stroke Clinic II June 14-17 10:30 – 11:30 am
Stroke Clinic III June 21-24 10:30 – 11:30 am  

*Friday of each week will be used as a make-up for any day cancelled due to inclement weather.
