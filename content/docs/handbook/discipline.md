---
weight: 5
bookFlatSection: true
title: "Discipline Problems"
---

# Discipline Problems  

Swimmers must maintain good self-discipline. During practices or meets, if a swimmer misbehaves excessively,  a coach and/or board member will contact the swimmer’s parents. If the swimmer’s behavior does not improve,  he/she will be dropped from the team. Under this  circumstance, registration fees will not be refunded. This is entirely at the discretion of the board and/or coaches.
