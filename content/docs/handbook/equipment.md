---
weight: 4
bookFlatSection: true
title: "Equipment"
---

# Equipment

**TEAM T-SHIRTS**
All Barracuda swimmers will receive a team T-shirt with  their registration. Team T-shirts will be available for  purchase after the first week of practice to ensure all of  our swimmers have received one with their registration. Be aware of sizes when registering you swimmer.  Youth and adult sizes are available.

**SWIM CAPS**  
Only LCST swim caps may be worn during swim meets.  Each swimmer will be given one before the first meet of the season. Additional caps are available for purchase under the Barracuda tent throughout the season.  

**SWIMSUITS**  
The team has chosen competition swimsuits, which  may be worn to practices and meets. These particular  suits are NOT required; but they do conform to swimming rules, look great and provide less drag than commercial suits. **While the team suit is optional, the team requires girls to wear one-piece swimsuits suitable for competition to all practices and meets.**
Swimsuit sales will be scheduled, and an email will be sent with information about when the vendor will be at the pool during practices.

**ACCESSORIES**
At all swim meets, caps & goggles will be available for sale. Only a Barracuda swim cap may be worn at swim meets. Barracuda merchandise will be for sale under the Barracuda tent.
