# Introduction

This is the official handbook of the League City Barracudas swim team.  The intent of this handbook is to inform swimmers and  parents of general information about the team and the  season, as well as our relationship with Clear Creek Swim  League and our expectations in regard to safety, conduct  and participation. Familiarity with the information herein  will make the family’s participation more enjoyable. 

This handbook does not include Clear Creek Swim  League rules and regulations, rules for intra-league  competition, technical rules for properly executing  strokes, cut-off times (Reserve and Champ), or league  records. This information is available in the current Clear Creek Swim League Handbook, which can be found on their website.  
  

## How to use this handbook

Use the left menu to select a section of the handbook you are interested in reviewing. If you are new to the team we recommeded carefully reading each section of this handbook and refer back as needed.

{{< hint info >}}
HINT: You can bookmark this page or any page within this handbook to come back too later.
{{</ hint >}}
