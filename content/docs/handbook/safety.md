---
weight: 1
bookFlatSection: true
title: "Safety Guidelines"
---

# Safety Guidelines

The safety and well being of your child is of the utmost concern to us. The following guidelines have been set to help ensure their safety. Please discuss these rules  with your children so that everyone understands what is expected.

## Swimmers 8 & Under

1. Children may not be dropped off and left alone at  the pool.
1. If you cannot be with your children, please make  arrangements with another parent or guardian  (at least 15 years old) to look after them during a  practice or meet. Swimming is different from most  team sports in that there are many more children  than coaches. Please know where your child is at  all times, including when in the water.

## All Swimmers

1. If dropped off, go immediately inside the fence to the  pool area. All children must stay in the fenced pool  area during practice.
1. Do not climb on the lifeguard stands. Hometown  Heroes Pool rules must be observed at all times.  
1. All trash must be picked up after practices and home  
meets. The City will be charging the team for clean  up if this is not done.
1. A swimmer should inform one of the coaches if he/ she leaves the pool to go to the restroom or must  leave practice early. Only restrooms at the pool are  to be used.
1. Swimmers must not enter the pool before their  scheduled practice time and should get out as  soon as their age group is released unless given  prior permission by the head coach to start early or  remain late.
1. Swimmers are not allowed in the Hometown Heroes  Park building when wet. This is a city rule!
1. Please be prompt in picking up your swimmer. For  their safety, you must come inside the fenced  pool area to get them. This brief visit also helps  parents keep up with team information.
1. During a meet, no one is allowed in any part of the competition pool unless they are participating  in an event or using a warm up lane, if provided  and supervised. This includes dangling feet in  areas not used for competition. Failure to follow  this rule could result in your swimmer being  removed from the meet.
1. No alcoholic beverages or tobacco products are  allowed at practices or meets.

## Baby Pool

With City enforcement that both pools require lifeguards, the Baby Pool will **NOT** be open during practices or meets. No parents, swimmers, or siblings are allowed in the Baby Pool/Splash Pad at any time.
