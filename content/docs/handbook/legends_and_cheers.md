---
weight: 5
bookFlatSection: true
title: "Barracuda Legends & Cheers"
---
  
## The Legend of the Cabbage  
  
Once Upon a time, in the far away land of League City, there swam some very excited and energetic Barracudas. They would practice every day, trying to get reserve and champ times and win the most points they could for their team.  Across from their pool, there was an old farmer, who planted all sorts of lettuces, but most of all cabbage. He planted so much cabbage that he could not pick them all and the cabbage began to rot and smell. As the little Barracudas were practicing day in and day out, they began to smell the strong odor coming from the farmer’s field, and they began to swim faster and take fewer breaths, desperately trying not to smell the cabbage.  

Soon, the Barracudas realized that the cabbage smell had helped them become very speedy swimmers and win many championships. To this day, all Barracudas remember how “cabbage power” helped them to become the fastest swimmers in the land.
  
**The Barracudas are bound with Cabbage Power and  we give our swimmers cabbage before every meet.**
  
## Barracuda Team Cheers  
  
**DUNK-A-HEAD**  
Dunk-a-head, dunk-a-head
Black and red  
Dunk-a-head, dunk-a-head
Cabbage fed.  

**DAFFY DUCK**
My name is Daffy Duck
I live on a merry go round
I play all day, in the merriest way
And this is how it sounds
Toot, toot, toot, toot!
  
**DYNAMITE!**  
Coaches> Barracudas are what?  
Kids>Dynamite  
Coaches> Barracudas are what?
Kids>Dynamite
All> Barracudas are...
Tick tick tick tick, **BOOM Dynamite!**

**UH UNGOWA**
Uh, ungowa, we got that cabbage power
*repeated many times with a dance*
