---
weight: 8
bookFlatSection: true
title: "Need to Speak to a Coach?"
---

# Need to Speak to a Coach?  
If you have any questions or concerns during practice or at a meet, we ask that you please direct them to a board member. The coaches are responsible for a large number of kids at practices and meets and will not be able to provide the assistance you need. If it is a coaching issue, the board will go to the appropriate coach for your answers.   
  
Coach Berg: aquaticscoach8283@gmail.com  
Coach Hannah: hekapfer99@gmail.com
