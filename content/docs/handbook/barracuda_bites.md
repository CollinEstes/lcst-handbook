---
weight: 11
bookFlatSection: true
title: "Barracuda Bites"
---

# Barracuda Bites

The Barracuda Bites newsletter is your weekly connection to all that is going on with the Barracudas  during that week! Your family will receive an email each  week of the season with the Bites attached. Bites are also uploaded to the website under the Documents tab.  If you have questions on the Bites, please see a board member at the pool.
