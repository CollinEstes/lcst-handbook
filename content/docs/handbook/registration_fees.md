---
weight: 2
bookFlatSection: true
title: "Registration Fees & Refunds"
---

## Team Membership Fees

- **Swimmer**: $150.00 each
- **Active High School Swimmers**: $85.00/each
- **Active USA Swimmers**:  $85.00/each
(plus $5 Hytek Administration Fee/Athlete)

{{< hint info >}}
The maximum fee is $400.00 per family. (Three or more swimmers)
{{< /hint >}}

A late registration fee of $25.00 is charged for each swimmer whose fees are not paid by the first day of practice.
No swimmer with unpaid registration fees may practice or swim in a meet.

Only swimmers having paid their fee will be added to the official team roster. This roster is an official league document. League rules prohibit entrants not on a team roster.  
  
## Tryouts
  
All new swimmers **must** try out for the team. In order to join, a swimmer must be able  to safely swim 25 yards unassisted. The swimmer’s ability will be judged solely by the coaching staff and/or team board. Try-outs are not needed for any competitive USA registered swimmer or a swimmer that has been on another CCSL team.

Tryouts for the 2022 season will take place on Monday and Tuesday, April 25th  & 26th at Hometown Heroes Pool from 5-6:30 pm. (only need to attend one)  

- Entry to pool will be via the front of the building.  
- Proof of age required – photo of birth certificate on phone is acceptable.
- Swimmer will need to come wearing suit and ready to get into the pool.
- Swimmer should bring towel and goggles.
- Swimmer will swim per coaches' instructions.
- Swimmer will be free to leave once the tryout is completed.

## Swimmer's Age  
  
A swimmer’s age for purposes of competition is his age on May 1st. A swimmer does not change age groups during the season. All new swimmers must be at least  six (6) years of age, as of May 1st, and be able to swim the required 25 yards. We will be accepting five (5) year old siblings of any returning Barracuda families. Proof of age will be required for new swimmers.
  
## Refunds

Refunds will be made through the first week of practice with a $30 administrative fee per swimmer deducted.  After that, no refunds will be paid.
