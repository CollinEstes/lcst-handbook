---
weight: 7
bookFlatSection: true
title: "Spirit Nights"
---

# Spirit Nights

Let’s gather together and show our Barracuda pride while supporting local businesses. A percentage of your purchase will go back to the Barracuda’s. Make sure you tell everyone you know to support the Barracuda’s!

A complete list of Spirit Nights will be emailed and put in the Barracuda Bites Newsletter
