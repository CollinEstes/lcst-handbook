---
weight: 3
bookFlatSection: true
title: "Volunteer Requirements"
---

# Volunteers

Because there are so many jobs to be done, parent  participation is **mandatory** at a swim meet. The  Barracudas must furnish more than 75 workers for each  away meet and at least 125 workers for each home  meet. Every family is asked to volunteer at least 3 times during the swim season. Families with more than one  swimmer will be responsible for volunteering 5x during the season. An email will be sent out inviting your family to register for various volunteer shifts. Once you have selected your 3 choices, your registration status will be approved. The week prior to your shift, you will receive an email reminder. You must check in with the volunteer coordinator prior to the start of each swim meet. If you fail to show up for your designated shift, your child may not be able to swim in the next swim meet.  

![Screenshot of Volunteer Selections](/images/volunteershifts.jpg)

## Family Requirements

Families are asked to volunteer during the swim season as follows:

- **Single Swimmer Families**: at least 3 times
- **Families**: at least 5 times

{{< hint danger >}}
NO CHILD WILL BE CONSIDERED REGISTERED AND ABLE TO START PRACTICE UNTIL YOUR REGISTRATION, VOLUNTEER FORM AND PAYMENT ARE RECEIVED!
{{</ hint >}}

## Volunteering Process

### Selection and Registration

An email will be sent out inviting your family to register for various volunteer shifts. Once you have selected your 3 choices, your registration status will be  approved.

### Week of the Meet

The week prior to your shift, you will receive  an email reminder. You must check in with the volunteer coordinator prior to the start of each swim meet.

{{< hint danger >}}
If you fail to show up for your designated shift, your child may not be able to swim in the next swim meet.  
{{</ hint >}}

## Positions

Positions, which must be filled at a swim meet, are briefly described below. These positions are divided into  three groups, depending on the amount of training or experience that is needed.

Swimming is a very volunteer-oriented sport. Our children  would not get to enjoy this wonderful sport without the  efforts of their parents. One of the crucial roles parents play is officiating. The reason I originally started officiating is that I wanted the best spot on the deck to watch my kid swim. You can't get any closer! The other benefit that the Barracudas give is the relief from any other volunteer requirements. That's right, if someone in your family  officiates, you don't have to volunteer for anything else!  

**The training clinics are FREE!**

## Trained Positions

For the following jobs, specialize training is required.  The CCSL provides clinics in the spring to train parents  to fill these positions. They require a rather complete  understanding of competitive swimming.

{{< hint info >}}
Not up for training? Check out the [No Experience Required section below](#no-experience-required-positions)
{{</ hint >}}

### Team Clerk of Course

Handles changes to meet entry cards, entry lists,  and heat assignments resulting from scratches and  substitutions. The Clerk of Course also determines  the legality of swimmer entries for both teams, and  helps distribute entry cards to swimmers.

### Stroke and Turn Judge

Judge the technical form of strokes and turns  according to League rules and disqualify swimmers  for violations. Must wear solid white shirts or CCSL  official shirt and navy blue shorts/skorts.

### Starter

Calls swimmers to the starting block, starts each  heat, and handles disqualifications on starts. The  Starter can also work as a Stroke and Turn Judge at  the direction of the Referee. Starter positions require  prior certification and experience as a Stroke and  Turn Judge.

### Referee

Is the senior official at a meet and has full and final  authority in all decisions and rule interpretations.  Referee positions require prior certification and  experience as a Stroke and Turn Judge and Starter.

#### League Training Clincs

Our primary job is to make sure the competition is fair  for all of the swimmers. We do that by making sure the  swimmers are doing the strokes as the rules state and  that no one gets an advantage. There is a training session  and test involved.

## No Experience Required Positions

The following positions require **no previous experience or training**:

### Meet Volunteer Coordination Team

Coordinates the staffing and schedule of all volunteers necessary for swim meets.

### Set up and Tear Down

Help prepare the pool area for the swim meets and pack away after meets (home meets only). Coordinate clean up of the pool area after home meets. (All Team members and parents are asked to help clean up after a home meet.)

### Concession Stand

Help prepare orders and/or collect money. Also may be asked to deliver cold drinks to officials and workers during the meet. The last shift of the day helps clean the refreshment area (home meets only).

### Runners

Responsible for collecting lane sheets and DQ slips from the timers and deliver them to the scorers.  Also be responsible to help moving the 8 and under swimmers to the other side of the pool for some heats.

### Ready Area

Help arrange, organize and prepare children for races.

### Head Timer

Coordinates the timers from both teams. Insures that  all lanes are filled with the proper number of timers.  At home meets will conduct a meeting in the morning  with both teams. The head timer also starts back-up stopwatches.

### Timers

Time swimmers using digital stop watches and record the time on the swimmer’s entry card. There are three timers per lane.

### Head Scorer

Coordinates data entry, validates records with referee,  resolves questions regarding DQs, validates scoring  
and certifies official meet results.

### Scorers

Enter swimmers’ official times, disqualifications,  and scratches in the meet management computer. Validate record performances, calculate team scores and post official heat results. You should be comfortable working with numbers and computers.

### Head Ribbon Person

Coordinates ribbon writing for all meets.  

### Ribbon Labeling Team

Receive labels from the scorers and place on the back of the ribbons for all swimmers to be filed and distributed at a later date.

### Stroke Clinic Coordinator

Promotes, schedules and coordinates participation in special small group clinics to improve stroke technique.

### Ribbon Distribution Team

Distribute ribbons during practices one day week following the meet.

### Champ Series Signup Team  

Assist swimmers in filling out entry forms and  making payments to enter the champ series meets.

### Banquet Team

Organize and arrange the awards banquet.

### Merchandise Team

Responsible for selling Barracuda items one  day a week at practice and at swim meets.
