---
weight: 3
bookFlatSection: true
title: "2022 LCST Board of Directors"
---

# 2022 League City Barracuda Board of Directors  
The names, email addresses and telephone numbers of the 2022 League City Swim Team Board of Directors are provided below. If you would like to volunteer in any way, please contact any board member below.  

| Position | Name | Email | Phone |
| -------- | --------- | --------- |--------- |
| President | Lauren Estes | lauren.e.estes@gmail.com | 409-392-6722 |
| Vice President | Stefani Taylor | stefani.toungate@yahoo.com | 713-397-5110 |
| Treasurer | Cherie Oubre | cherieoub@aol.com | 409-718-5148 |
| Secretary | Robyn Ethridge | robynethridge@hotmail.com | 281-381-8220 |
| Clerk of Course | Lisa Bunge | cudaclerk@gmail.com | 832-444-4732 |
| Officials | Dan Bunge | bunged_1@yahoo.com | 832-444-4732 |
| At Large | Stacey Armstrong | stacey.armstrong44@gmail.com | 832-528-2345 |
| At Large | Ashley Gremillion | ashleygrem@me.com | 832-385-6963 |
| At Large | Michelle Kapfer | thekapfers@gmail.com | 832-563-5463 |
| At Large | Cullen Oubre | oubrec@aol.com | 409-718-5144 |
