---
weight: 2
bookFlatSection: true
title: "President's Letter"
---

# Letter from the President

**Dear Barracudas,**

Welcome to the 2022 League City Barracudas Swim Season!  My name is Lauren Estes, and I am proud to be serving as your Barracudas President this year.  My family joined the Barracudas in 2016, and my sons Aiden & Charlie both swim for the team.  I can remember being a little intimidated by the size of the team at first, but by the end of that season I knew that we had found something really special.  We see the Barracudas as a way for our family to give back to our community, and we look forward to it every year. 

Your 2022 Barracuda Board members have been meeting monthly since August to prepare for this swim season, and we are excited to see all of our “Barracuda Family” back at the pool this summer. We strive to make it the best experience for both parents and swimmers alike. If you are new to the team, welcome to the family!  The friendships you make while being a Barracuda will last a lifetime. If you have any questions at any time, please let me or any board member know.  During the season I am available by phone, text or email.  

This year will host 3 swim meets. In order to make these meets function, we need you to complete your volunteer sign ups. Our volunteers are what make this team run! We can’t possibly do this without you! Go to our website, www.lcbarracudas.org and click the parent portal tab. You will see the volunteer opportunities for meets as well as our concession needs through this tab on the right side of the screen.

Please be sure to read this handbook thoroughly.  A few important reminders include:

- During the season we send out a weekly email called “Barracuda Bites” which contains important updates about what is happening with the team.  Make sure to check your email frequently.
- No parents or siblings will be permitted on the deck at any time during practices.  
- No child 8 and under can be dropped off.  Parents MUST park and walk swimmers in and remain on the premises for the duration of the 8U swimmers’ practice.  
- Entrance to the pool: All swimmers and parents MUST walk through the building to get to the pool.  
- Sidewalk chalk is NOT permitted per city guidelines.
- Please clean up after your family at practices and meets.  
- The baby pool is completely off limits during practices and meets. No Barracuda swimmers are allowed in the baby pool at any time.
- Our volunteers are what make this team run!  We can’t possibly do this without you!  Go to our website, www.lcbarracudas.org and click the parent portal tab.  You will see the volunteer opportunities for meets as well as our concession needs through this tab on the right side of the screen. If you do not have your volunteer requirements prior to the beginning of meet season, your swimmer will be entered into the meet.
- Meet parking is in the big parking lot by the play set/soccer fields.  NO PARKING is permitted under the driveway at the front doors.  During meets only, pool access will be via the side gate closest to the play set.

It's going to be a fantastic season!

**Lauren Estes**  
**President, LCST Barracudas**  
**Cell: 409-392-6722**  
**Email: lauren.e.estes@gmail.com**


 

