---
weight: 4
bookFlatSection: true
title: "2022 Committee Chairs"
---

# 2022 Barracuda Committee Chairs  
There are still positions available. If you would like to volunteer for any openings listed below or know of another skill that would benefit the team, please contact any board member.  
  
| Position | Name | 
| -------- | --------- | 
| Covid Coordinator |
| Champ Series Liaison |
| Concessions |
| Fun Fridays |
| Handbook Design/Layout |
| Merchandise |
| Pictures |
| Scoring |
| Season Kick Off Party |
| Set-up/Tear Down | Cullen Oubre |
| Sponsors |
| Spirit Nights |
| Stroke Clinic |
| Swimsuits/T-shirts/Caps |
| Team Representative | Stefani Taylor |
| Volunteers |
| Officials | Dan Bunge |
| Social Media/Advertising |
| Lifeguards |
| Coaches |
| CCSL Representatvies | Stacia Lauderdale & Paige Tucker |
| Banquet | Robyn Ethridge |
| Audit |
| Clerk | Lisa Bunge |

