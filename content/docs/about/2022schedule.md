---
weight: 2
bookFlatSection: true
title: "2022 Schedule"
---
  
## 2022 Schedule  
  
Keep up to date with the latest news and changes at www.LCBARRACUDAS.org  
  
### Important Dates  
  
| Event | Date | Location | Information |
| -------- | --------- | --------- | --------- |
| Tryouts | April 25 & April 26 | Hometown Heroes Pool (HHP) | link to tryout info |
| First day of Practice | May 2 | Hometown Heroes Pool (HHP) | link to practice info |
| Parent Meeting | May 11 | Under the Awning at HHP | We will have a parent meeting @ 5:15 & @ 6:15.  You only need to attend one of these times.  This will be the only parent meeting of the season, please make every effort to attend.|
| Team Suit Sales | May 11 | Under the Awning at HHP | Swim Shops of the Southwest will have the Barracuda Team suit available to purchase at a discounted rate.  Team suits are encouraged, but not required.| 
| Team Pictures | May 25th | HHP | Team Picture at 6:00pm |
| Barracuda Banquet | July 9th  | HHP | 6:30pm |  
   
  

### Meet Schedule  
| Meet | Date | Team |
| -------- | --------- | --------- |
| HOME | June 4 | LCST vs Deerpark
| AWAY | June 11 | LCST vs Friendswood |
| HOME | June 18 | LCST vs Pearland |
| HOME | June 25 | LCST vs Clear Lake Forest |
| AWAY | July 9 | LCST vs PineBrookWood |
  
*Swimmer check in at 6:30 am – Meets begin at 8:00 am  
Check website for directions and parking information.  
  
### Last Chance Meet  
July 12th in Baytown
  
### Champ Series
  
| CCSL Champ Series | Date | Location |
| -------- | --------- | --------- |
| Reserve Meet | July 16 | Location TBA |
| Champ Meet | July 17 | Location TBA | 

Dates & Times are subject to change. Watch for info via emails.  See Meet section for more information regarding meets.


### PRACTICE SCHEDULE 
PRACTICES ARE IMPORTANT Swimmers will benefit greatly by regular attendance at practice sessions. At practices, they receive  instruction in the competitive strokes and are given an opportunity to build physical endurance. Swimmers also can keep up with the latest news as information is distributed at practices. 
  
No Barracudas are allowed in the pool unless they are participating in practice. No unattended children are allowed at any practice or swim meet. THE CITY  REQUIRES THAT SWIMMERS REMAIN OUT OF HOMETOWN HEROES PARK BUILDING AT ALL TIMES. 
All practices are held at Hometown Heroes Pool. Practice is from Monday through Friday. Practice times are listed below. **THERE IS NO PRACTICE ON MEMORIAL DAY.**  
### Afternoon Practices  
**MAY 2nd through MAY 30th**  
High School Kids 4:00 – 5:00 pm  
8 & Under 5:00 – 6:00 pm  
9-14 yrs (Non-High School) 6:00 – 7:00 pm  
  
### Morning Practices  
**May 31st through July 8th**  
High School Kids 7:30 – 8:30 am  
9-14 (all non HS) 8:30 – 9:30 am  
8 & unders 9:30 – 10:30 am    
  
### Practices for Last Chance, Reserve & Champ Meet  
**July 11th through July 15th**
13 & Up - 8:00 - 9:00 am
12 & Under - 9:00 - 10:00am
  


