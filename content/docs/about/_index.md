 ![2022 Barracuda Handbook](/images/2022barracuda.jpg)
  
# Overview

The League City Barracuda Swim Team is a non profit, tax-exempt organization, with a mission to field and support a competitive summer swim team. Our goal is to provide a quality program for the promotion, growth, training, enjoyment, good sportsmanship, and participation of children and youth in the sport of swimming.

The management of the team is vested in the Board of Directors and its officers. In August, the Board begins planning for the next swim season. Board members  meet regularly to represent the team to the Clear Creek Swim League, manage the budget, evaluate and hire coaches, arrange for the pool and related facilities, procure and inventory equipment, coordinate  registration, schedule practices, and manage ordinary business and administrative functions of the team. 

Beginning in May, coaches will hold daily practice after school to improve stroke technique and build endurance needed for competition. These practice sessions are broken down into appropriate age groups. As soon as CCISD schools are dismissed for the summer, practice will switch to mornings. Meets will begin in June and be held every weekend through the end of June/beginning of July.  Post-season meets for those swimmers that meet or exceed cut-off time standards will be held following the regular season.  
