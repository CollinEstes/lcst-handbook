# LCST Handbook

This project is built with an open source website code framework [Hugo](https://gohugo.io/).  It is a free to use, open source technology that makes building handbooks and other documentation like websites extremely easy.

On top of this open-source technology, Hugo, software developers build general purpose "UI templates" that provide you with a simple set of code snippets that make it easy to have some customized user interface elements to build your website with.  

For this project we are using the ["Hugo Book"](https://github.com/alex-shpak/hugo-book) theme. 

## How this handbook works

This handbook is a project, which simply means it is a collection of folders and files.  There is only one area of this project that concerns the actual content of the handbook, luckily it is named [/content](/content/).  If you didn't click that link in the previous sentence, you can also find the ["/content"](/content/) folder through the project folder view above.

## What is ".md"

Looking through the ["/content"](/content/) folder you will notice that all the files end in `.md`.  This is a super simple format called [markdown](https://www.markdownguide.org/cheat-sheet/).  Markdown lets you do headings, lists, links, and tables which make it easy to create new interactive handbook for our Barracuda families.

Everything you need to know to write markdown:

- heading are "#", the more you have the lower the heading think (#=heading1, ##=heading2)
- lists are exactly like this one with a "-"
- You can always preview before you check in your change, look for "Preview Markdown" where you are editing the markdown file.

## What are the "_index.md" files about?

Looking through the ["/content"](/content/) folder you will notice that there are serveral files that are named "_index.md".  This is special to our framework Hugo and the Hugo-Book theme we used for this site.  But each "_index.md" file is the home page for folder it is in.  This is what people see when they load the home page of the site, or click the main headings like ["about"](/content/about/) or ["handbook"](/content/handbook/).

## How to make the PDF

This part currently requires a developer

```bash
docker run -it -v $PWD:/app --entrypoint /bin/sh  pandoc/latex
cd ../app
pandoc FULL_HANDBOOK.md -o FULL_HANDBOOK.pdf
``` 